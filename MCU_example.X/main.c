/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#include <xc.h>         /* XC8 General Include File */

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp */

/******************************************************************************/
/* User Global Variable Declaration                                           */
/******************************************************************************/

uint8_t counter;

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/
void main(void)
{
    /* Ensure the Watchdog Timer is Disabled */
    WDTCONbits.SWDTEN = 0;

    /* Configure the oscillator for the device */
    ConfigureOscillator();

    /* Initialize I/O and Peripherals for application */
    InitApp();

    /* Enable Time Critical Modules */
    T2CONbits.TMR2ON = 1;   // TIMER 2 Module

    while(1)
    {
        /* Time-Invariant Operations,
         * Watchdog Timer Servicing
         */
    }

}

