/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

#define LED_ON      (0)
#define LED_OFF     (1)

/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/

/* TODO User level functions prototypes (i.e. InitApp) go here */

void InitApp(void);         /* I/O and Peripheral Initialization */
void InitRC5 (void);
void InitTimer2 (void);