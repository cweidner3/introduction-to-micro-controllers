/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#include <xc.h>         /* XC8 General Include File */

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#include "user.h"

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/* Baseline devices don't have interrupts. Note that some PIC16's 
 * are baseline devices.  Unfortunately the baseline detection macro is 
 * _PIC12 */
#ifndef _PIC12

void interrupt isr(void)
{
    if(PIR1bits.TMR2IF)
    {   // Interrupt for TMR 1
        if(PORTCbits.RC5 == LED_ON)
            PORTCbits.RC5 = LED_OFF;
        else
            PORTCbits.RC5 = LED_ON;

        // Clear the interrupt flag
        PIR1bits.TMR2IF = 0;
    }
}
#endif


