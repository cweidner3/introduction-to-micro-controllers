/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#include <xc.h>         /* XC8 General Include File */

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#include "system.h"

/* Refer to the device datasheet for information about available
oscillator configurations. */
void ConfigureOscillator(void)
{
    /* Configure the Oscillator, pg 65 in Datasheet */
    OSCCONbits.SPLLEN = 0;  // 4x PLL Disabled
    OSCCONbits.IRCF = 0x0;  // Frequency 31 kHz
    OSCCONbits.SCS = 0x2;   // Use Internal Oscillator
}
