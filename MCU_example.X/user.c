/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

#include <xc.h>             /* XC8 General Include File */

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */

#include "user.h"

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

/* <Initialize variables in user.h and insert code for user algorithms.> */

void InitApp(void)
{
    /* Initialize All Ports as Digital Inputs */
    TRISA = ~0;
    TRISC = ~0;
    ANSELA = 0;
    ANSELC = 0;

    /* Initialize peripherals */
    InitRC5();
    InitTimer2();

    /* Enable interrupts */
    INTCONbits.GIE = 1;     // Enable interrupts in INTCON register
    INTCONbits.PEIE = 1;    // Enable Periphreal Interrupts
}

/**
 * Configure PORT RC5 as Digital Output for LED Controll
 */
void InitRC5 (void)
{
    /* Configure pin direction */
    TRISCbits.TRISC5 = 0;
    PORTCbits.RC5 = LED_OFF;
}

/**
 * Configure the Timer 2 Module (8-bit Timer)
 *
 * Timer 2 Clock: 125 kHz = Fosc/4
 * Desired Frequency: 2 Hz = 125e3 / 62500
 * Period Value: 62500
 */
void InitTimer2 (void)
{
    /* Timer Clock Configuration */
    T2CONbits.T2CKPS = 2;   // Use 1:16 Prescale

    /* Load the Timer Period */
    PR2bits.PR2 = 242;

    /* Enable Timer 1 Interrupt */
    PIE1bits.TMR2IE = 1;

    // Timer 1 will be enabled just before while(1) in main.c
}