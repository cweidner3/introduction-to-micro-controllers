% Document Type and Dimensions
\documentclass[12pt, titlepage, a4paper]{article}
\usepackage[a4paper,left=2cm,right=2cm,top=2.5cm,bottom=2.5cm]{geometry}

% Setting the page dimensions
%\oddsidemargin 0pt
%\topmargin 0pt
%\headheight 15pt
%\headsep 25pt
%\textheight 8.25in
%\textwidth 6.3in
%\marginparsep 0pt
%\marginparwidth 0pt
%\footskip 30pt

% Random Packages
\usepackage[none]{hyphenat} %No Word-wrap hyphens
\usepackage{ragged2e} % RaggedRight and such
\usepackage{changepage}
\usepackage{amsmath}
\usepackage{multicol} % Muli-Column
\usepackage{titlesec} % Spacing for sections and subsections
%	\titlespacing*{\subsection}{0pt}{2pt}{0pt}
%	\titlespacing*{\section}{0pt}{10pt}{2pt}
\usepackage{pdfpages}
\usepackage{lastpage} % Used for getting the last page number in the header
\usepackage{titlesec} % Changing the size of the section titles (and other titles)
\usepackage{lipsum} % For dummy text (Not really needed)
\usepackage[linktoc=all]{hyperref} 
\usepackage{hypcap}
\usepackage{fixltx2e} % for \textsubscript{}
\usepackage{paralist} % Inline lists
\usepackage{listings}
\usepackage [english]{babel}
\usepackage [english=american]{csquotes}
\usepackage{sectsty}

\MakeOuterQuote{"}

% Header Formatting
\usepackage{fancyhdr}
\pagestyle{fancy}
\lhead{}
\chead{An Introduction to Micro-Controllers}
\rhead{}
\lfoot{\leftmark}
\cfoot{Page \thepage\ of \pageref{LastPage}}
\rfoot{Corbin Weidner}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

% Formatting for within the document
\setlength{\parindent}{0pt} % No indents for paragraphs
\titleformat*{\section}{\LARGE\bfseries}
\titleformat*{\subsection}{\Large\bfseries}
\titleformat*{\subsubsection}{\large\bfseries}
\numberwithin{equation}{section} % Options (section, subsection)
\numberwithin{figure}{section} % Options (section, subsection)
\hypersetup{pdftex,colorlinks=true,linkcolor=black,citecolor=black,urlcolor=blue}

\begin{document}

%% Title Page
\title{An Introduction to Micro-Controllers\\(Microchip PIC Focus)}
\author{Corbin Weidner}
%\date{July 14, 2014}	% Defaults to \today
\maketitle

%% Abstract
%\begin{abstract}
%	\lipsum[1]
%\end{abstract}

\setcounter{page}{2}
\tableofcontents

%% Document Content Begins Here
\newpage
\section{Introduction}
\markboth{Introduction}{}
	This document is designed to provide an introduction to designing and programming application circuits with micro-controller units (MCU). 
	The intended audience is directed towards the student or hobbyist interested in micro-controllers and has a familiarity with the C programming language. 
	The document starts by informing the audience about the types of documentation and how it will be used to design for the micro-controller in mind.
	It will then cover how to create the application circuit from the information provided in the documentation and the programming connections and will focus on using the PIC16F1823 device by Microchip.
	The following sections cover the software required and the programming of the micro-controller.
	An appendix is given which supplies the sample program covered in the later parts of the document as a reference for the audience.
	

\subsection{A Note to Students}
	Some micro-controller manufacturers offer discounts and waived-fee samples of their micro-controllers to those with an official edu email address. 
	It is recommended to check out these deals with Microchip, known for their PIC MCUs, and Atmel, their ATmega MCUs provides the basis of the Arduino, since their micro-controllers are popular with hobbyists and will in return have a large user-base.

	
\newpage
\section{Getting the Documentation}
\markboth{Documentation}{}
	The documentation for the micro-controller is critical for creating designs involving the particular micro-controller. 
	What documentation should we download from the product page? 
	\begin{itemize}
		\item Datasheet
		\item Technical Reference Manual (TRM)
		\item Errata
	\end{itemize}
\subsection{The Datasheet}

	The datasheet details the micro-controller and the modules included inside the micro-controller. The datasheet will be divided into sections with each section containing a description of the module and the registers related to the operation of that module. 		
	Fig.~\ref{fig:pin_diagram} and Fig.~\ref{fig:porta_reg} depict some of the information that can be obtained in the device datasheet.\\

\begin{figure}[h!]
	\centering
	\fbox{\includegraphics[width=5in]{images/PIC16F1823_Pin_Diagram.png}}
	\caption{Pin Diagram \cite[\sl p.6]{PIC16F1823_datasheet}}
	\label{fig:pin_diagram}
\end{figure}
	Fig.~\ref{fig:pin_diagram} will be useful when creating the circuit either on the breadboard for testing or when designing the schematic and creating printed circuit board (PCB) files. 
	Note that Microchip uses the notation for of V\textsubscript{DD} for the positive DC voltage supply for the micro-controller and V\textsubscript{SS} for ground voltage.\\

\begin{figure}[h!]
	\centering
	\fbox{\includegraphics[height=3in]{images/PIC16F1823_Alloc_Table.png}}
	\caption{Pin Allocation Table \cite[\sl p.7]{PIC16F1823_datasheet}}
	\label{fig:alloc_table}
\end{figure}
	Fig.~\ref{fig:porta_reg} depicts the register for PORTA, a control for the PICs general-purpose input-output (GPIO) module. 
	Registers are a means for controlling the modules within the micro-controller. 
	The pin diagram in Fig.\ref{fig:pin_diagram} and the pin allocation table in Fig.\ref{fig:alloc_table} are used together to determine the pin is allocated for a particular function of the micro-controller.
	In this case we can look to section 12 (I/O Ports) in the datasheet for information about setting up the I/O port for LED output control.\\

	 Above each bit or collection of bits (not displayed on this figure) provides information about what can be done with that bit of the register and what to expect from it. 
	 The "R" and "W" specify that the bit is readable and/or writeable. 
	 Note that above RA3 in Fig.~\ref{fig:porta_reg} states that it is only readable, this is because that pin is used for the PIC's reset feature ($\overline{\mbox{MCLR}}$ also noted as V\textsubscript{PP}) used during programming of the micro-controller.
	


\subsection{The Technical Reference Manual}
	The technical reference manual (TRM) gives a more detailed explanation of the modules, also known as peripherals, included in the micro-controller. 
	The TRM goes into more detail of the module's operation than the datasheet and may even include sample code for a particular mode of operation. 
	These documents are not always present for the micro-controllers in question as it is with the PIC16F1823.
\begin{figure}[h!]
	\centering
	\fbox{\includegraphics[width=5in]{images/PIC16F1823_PORTA_register.png}}
	\caption{PORTA Register \cite[\sl p.126]{PIC16F1823_datasheet}}
	\label{fig:porta_reg}
\end{figure}

\begin{samepage}
\subsection{The Errata}
	The device errata is used to bring forth information about differences between the datasheet and how the actual micro-controller works. 
	The manufacturer documents this information into the errata as they gain knowledge of the particular problem or quirk and they will also publish possible workarounds \cite[\sl p.9]{PIC16F1823_datasheet}.
	This information can be critical to the success of the project and a quick glance at this document can save many hours of troubleshooting.\\

	An example of this problem can be noted in the PIC16F1823 errata on page 7 with the EUSART module. If the baud rate generator data register is loaded with zero in 16-Bit high-speed asynchronous mode, the module may not work properly \cite[\sl p.7]{PIC16F1823_errata}. 
	A problem like this could possibly give incorrect data to another micro-controller in a design and would cause malfunction of the overall device.
\end{samepage}

\subsection{Application Notes}
	Application notes can also be a useful tool during the design process. 
	Application notes are written often times to explain a process of using a device or utilizing certain features of that device. 
	As an example Microchip's AN1302 details how to create an I\textsuperscript{2}C bootloader for the PIC16F1xxx series of micro-controllers.
	This information is not critical like the information given in the errata, but it can aid in the design process of create a bootloader.

\newpage
\section{Creating the Circuit}
\markboth{Creating the Circuit}{}
	It is a good idea when designing a circuit to create a schematic of the design. This way you may be able to come back to the same design later or reference the design when assembling the circuit.
	This task can be achieved either by drawing the circuit by hand on a piece of paper or by using a computer aided program (Eagle CAD, ExpressPCB, FreePCB)\cite{eevblog_wiki}.


\subsection{Simple LED Control Circuit}
	This example design has been designed with Eagle CAD software on the freeware license using a mix of the Sparkfun Eagle Libraries \cite{sparkfun_eagle} and custom libraries.\\

	The schematic shown in Fig.~\ref{fig:schematic} contains a custom part that resembles the PIC16F1823 micro-controller for an SOIC style package. When creating custom software parts of the micro-controller, the specifications of the package dimensions are often given at the end of the datasheet. For the PIC16F1823 the SOIC package is described as well as a given recommended land pattern on pages 417-419 of the datasheet.

\begin{figure}[h!]
	\centering
	\fbox{\includegraphics[width=4in]{images/simple-PIC16F1823-schmatic.png}}
	\caption{Simple Schematic for LED Control}
	\label{fig:schematic}
\end{figure}

\subsubsection{Main Connections}
\begin{samepage}
	Reference the schematic in Fig.~\ref{fig:schematic}, the images in Fig.~\ref{fig:circuit}, and pin diagram in Fig.~\ref{fig:pin_diagram} when making the main connections listed below:
\begin{description}
	\item[$\bullet$] Voltage Supply (V\textsubscript{DD}) \hfill \\
		This connection supplies power to the micro-controller and the other circuitry such as the LED-resistor combination. This connection is made from the positive power rail on the breadboard (red line) in Fig.~\ref{fig:circuit} to pin one of the micro-controller according to Fig.~\ref{fig:pin_diagram}.
		
	\item[$\bullet$] Ground (V\textsubscript{SS}) \hfill \\
		This connection provides for a common low voltage for the whole circuit to reference \cite{allaboutcircuits}. This connection is made from the negative power rail on the breadboard (blue line) in Fig.~\ref{fig:circuit} to pin 14 of the micro-controller.
		
	\item[$\bullet$] Programming Connections (V\textsubscript{PP}, ICSPDAT, ISCPCLK)
		The programming connections for the Microchip PIC family of devices uses their In-Circuit Serial Programming (ICSP) style of connections. V\textsubscript{PP} should be "pulled-up" meaning that it should have a resistor (1 k$\Omega$ will work) connected to it and the V\textsubscript{DD} signal. These connections are shown in the schematic in Fig.~\ref*{fig:schematic}.
	
	\item[$\bullet$] Voltage Supply Decoupling Capacitor (Optional) \hfill \\
		This connection is not always required but is usually recommended for low-power and critical applications to keep the supply of power constant and smooth. This connection is made by placing the capacitor across the V\textsubscript{DD} and V\textsubscript{SS} lines. If using an electrolytic capacitor, make sure to orient the negative edge on the V\textsubscript{SS} line.
\end{description}
\end{samepage}

\begin{figure}[h!]
	\centering
	\fbox{\includegraphics[width=2.5in]{images/circuit-top-view.jpg}
	\includegraphics[width=2.5in]{images/circuit-side-view.jpg}}
	\caption{The Circuit (left), Side-View w/ Decoupling Capacitor (right)}
	\label{fig:circuit}
\end{figure}

\subsubsection{LED Connection}
\begin{samepage}
When using LEDs on microcontrollers, there are two different connection types.
\begin{description}
	\item[1.] {\sl Active-High} \hfill\\
		For this configuration, the LED is connected with the anode on the micro-controller pin and the cathode connected to ground (V\textsubscript{SS}).
		In this mode, the micro-controller GPIO pin supplies V\textsubscript{DD} to turn {on} the LED and supplies a ground voltage to turn {off} the LED.
		In active-high mode, the micro-controller will supply a current across the LED when the pin voltage is high.
	\item[2.] {\sl Active-Low} \hfill\\
		For this configuration, the LED anode is connected to the voltage supply line (V\textsubscript{DD}) and the cathode connected to the micro-controller pin.
		In this mode, the micro-controller GPIO pin supplies a ground reference voltage in order to turn {on} the LED and supplies V\textsubscript{DD} to turn {off} the LED.
		In active-low mode, the LED will be in the off state when the GPIO pin voltage is high causing no current to pass through. When the LED is in the active state, V\textsubscript{DD} will supply current to the LED.
\end{description}
\end{samepage}

In the example here, we are using the "active-low" configuration for the LED as shown in Fig.~\ref*{fig:schematic}. When connecting "through-hole" LEDs, there are two indicators of the LED cathode. 
\begin{inparaenum}[\itshape 1\upshape)]
	\item The side of the plastic package contains a flat section that signifies the cathode, or
	\item the shorter of the two leads signifies the cathode.
\end{inparaenum}

\begin{figure}[h!]
	\centering
	\boxed{
		\includegraphics[width=3in]{images/led.png}
	}
	\caption{The Through-Hole LED Package \cite{ledfig}}
	\label{fig:led}
\end{figure}

\newpage
\subsection{The Programming Tool}
\begin{samepage}
The programming tool used here to program the PIC micro-controller is Microchip's PICKIT 3 In-Circuit Serial Debugger and Programmer (ICSP).
The programming tool connects to the circuit at V\textsubscript{PP}, ICSP PGD and PGC, voltage, and ground pins.
The PGM pin on the PICKIT 3 (pin 6) is available for use during low-voltage programming.
In most cases, the PGM pin will not be used in the circuit design and can be omitted.
Fig.~\ref{fig:pickit3_pinout} shows how the programming tool connects to the application circuit with the first 5 pins on the PICKIT 3.
\begin{figure}[h!]
	\centering
	\boxed{
		\includegraphics[scale=0.3]{images/pickit3_pinout.png}
	}
	\caption{PICKIT 3 ICSP And The Connections \cite{pickit3_pinout}}
	\label{fig:pickit3_pinout}
\end{figure}
\end{samepage}

\newpage
\section{Downloading the Software}
\markboth{Software}{}
	Unlike the creation of analogue circuits, working with micro-controllers require the creation of a program that will be loaded onto the memory or storage of the MCU using the programming tool.
\subsection{The Compiler}
	Most, if not all, micro-controllers are supported by their manufacturer with a C programming based compiler. 
	At the time of writing the document, Microchip provides their MPLAB XC compilers for their 8-bit, 16-bit, and 32-bit micro-controllers. 
	The 8-bit PIC MCU compiler is Microchip's XC8 compiler which can be downloaded and installed from \url{microchip.com/compilers} under the free license. 
	The "Standard" and "PRO" licenses offer higher levels of optimization that can reduce the final binary size of the program \cite{microchip}. 
	The "Standard" version can provide 20\%-25\% reduction and the "PRO" version can provide better than 50\% reduction of the final program size \cite{microchip}.
	Reduced file size of the program can allow for larger programs to be made or longer data lengths to be acquired. 
\subsection{Using the Integrated Development Environment}
	The integrated development environment (IDE) for the PIC MCUs is Microchip's MPLAB X. 
	An IDE designed for the micro-controller will often include compiler specific syntax highlighting and code completion for micro-controller structures defined by the hardware abstraction layer. 
	An additional feature of an official IDE for the micro-controller is that it is designed to integrate with the programmer such that the program can be compiled and loaded onto the micro-controller from within the IDE.
	
\subsubsection{Creating a New MPLAB X Project}
Below are steps for creating a new project using the PIC16 template provided with MPLAB X IDE. Given the programming tool is PICKIT 3 and using an 8-bit PIC MCU.
\begin{enumerate}
	\item Create A New Project: File $\rightarrow$ New Project
	\begin{enumerate}
		\item Categories $\rightarrow$ Samples $\rightarrow$ Microchip Embedded
		\item Projects $\rightarrow$ PIC16 C Template
	\end{enumerate}
	\item Enter the Project Properties: File $\rightarrow$ Project Properties
	\item Create A New Configuration: Manage Configurations
	\begin{enumerate}
		\item New: Name (Default)
		\item Remove Other Configurations
		\item OK
	\end{enumerate}
	\item Select the new Configuration and Configure the Settings
		\begin{enumerate}
		\item Device: PIC16F1823
		\item Hardware Tool: PICkit3
		\item Compiler Toolchain: XC8
	\end{enumerate}
\end{enumerate}

\newpage
\section{Programming the Microcontroller}
\markboth{Programming}{}
\subsection{Hardware Abstraction Layer}
The hardware abstraction layer (HAL) provides an easy way for assigning values in the module registers of the micro-controller. Below are two methods of using the HAL to write and read from module registers when using the XC compilers.
\begin{description}
	\item {\sl REGISTERNAME} \hfill \\
		REGISTERNAME is the name of the register as shown in the datasheet such as shown in Fig.~\ref{fig:porta_reg}. Writing to the register in this fashion allows the modification of multiple bits at one time. The first example below sets RA0-RA2 GPIO pins to voltage high and sets RA4 and RA5 to voltage low. The second example sets RA5 to voltage high while keeping the other GPIO pins as the previous values. The third example shows how one can read only the value of RA5 from PORTA register. ({\sl NOTE: According to Fig.~\ref{fig:porta_reg}, RA3 will not be modified on write since it is a read-only value.})\\
		Example: 
		\begin{lstlisting}[language=c]
	PORTA = 0x7;
	PORTA = PORTA | (1<<5);
	gpioRA5 = (PORTA & (1<<5))>>5;
		\end{lstlisting}
	\item {\sl REGISTERNAMEbits.BITNAME} \hfill \\
		REGISTERNAME is the name of the register shown in the datasheet, similar to the previous example while BITNAME is the name of the specific bit within the register as defined in the register definition. The first example RA5 in PORTA register is being set to voltage high with no change to RA0-RA4. The second example shows an easy way of reading RA5 from the PORTA register. \\
		Example: 
		\begin{lstlisting}[language=c]
	PORTAbits.RA5 = 1;
	gpioRA5 = PORTAbits.RA5;
		\end{lstlisting}
\end{description}

\subsection{Blinking LED Program}
The structure of this program follows the PIC16 template supplied from MPLAB X. By using the PIC16 template, five source files 
\begin{itemize} \itemsep -5pt
	\item configuration\_bits.c
	\item main.c
	\item system.c
	\item user.c 
	\item interrupts.c
\end{itemize}
and two header files 
\begin{itemize} \itemsep -5pt
	\item system.h
	\item user.h 
\end{itemize}
are created along with project properties that will ensure reliable configuration.
\subsubsection{The Configuration Bits}
	In this step we will use a feature built into MPLAB X that helps to generate the source code for initial setup. 
	Navigate to "Window $\rightarrow$ PIC Memory Views $\rightarrow$ Configuration Bits". 
	A sub-window at the bottom of the screen will appear with the available fields that are configurable for the micro-controller.
\begin{description}
	\item [FOSC] 	: INTOSC \hfill \\
		This will set the oscillator selections to the internal oscillator in the MCU.
	\item [WDTE] 	: SWDTEN or OFF \hfill \\
		This will set the Watchdog Timer either off or controlled by software. By setting the Watchdog Timer to software enabled, it can be enabled after the initialization stage.
	\item [PLLEN] 	: OFF \hfill \\
		This will disable the phase-locked loop circuitry which can increase the oscillator output $4\times$ its initial value.
	\item [LVP]		: OFF \hfill \\
		This will disable the low-voltage programming setting of the micro-controller which is generally not required.
\end{description}

\subsubsection{Initialization Steps}
\underline{Oscillator Configuration:}\\
	After making sure the configuration bits are set correctly, we will set up the oscillator in the pre-defined function ConfigureOscillator() in system.c. 	
	We look to section 5.0 "Oscillator Module" for information on the configuration of the oscillator module. 
	After selecting the desired oscillator operating frequency we modify the SYS\_FREQ macro definition in system.h to represent this value.
\lstinputlisting[language=c, frame=L, firstline=12, lastline=20, numbers=left, firstnumber=12]{MCU_example.X/system.c}

\underline{LED Configuration:} \\
	According to the schematic in Fig.~\ref{fig:schematic}, we have set-up the LED to operate in active-low configuration on GPIO pin RC5. 
	In the LED configuration function InitRC5() in user.c, appendix~\ref{subsec:user.c}, we configure the GPIO pin as an output pin by setting TRISC5 to zero.
	Then we set the LED to an off state for initial value by setting RC5 in the PORTC register to one.
	To reduce the confusion of inverted notation, define statements have been created in user.h, appendix~\ref{subsec:user.h}, to specify the state the LED is on versus when the LED is off (LED\_ON and LED\_OFF).
\\~\\


\underline{Timer 2 Configuration:}\\
	To create a regularly occurring task to toggle the LED, we can use the TIMER 2 module in the PIC MCU.
	The timer module can be programmed to generate an interrupt sequence when the counting register matches the value stored in the period register shown in Fig.~\ref{fig:timerop}.
	Control of the frequency at which the timer overflows the period value is controlled by setting the prescale and period register values.
	In this example we use a 1:16 prescale value and a stored period value of 242 to approximate 8 Hz which will cause the LED to toggle to the on value at about 4 times per second.
	The C code is for this configuration is shown in appendix~\ref{subsec:user.c} lines 52-64.
\begin{figure}[h!]
	\centering
	\boxed{
		\includegraphics[scale=.5]{images/timer2_operation.png}
	}
	\caption{Timer 2 Operation}
	\label{fig:timerop}
\end{figure}


\subsubsection{The Main Loop}
	In the main.c source file shown in appendix~\ref{subsec:main.c}, the main() function is separated in to two sections: the initialization and the infinite while loop.
	The initialization section calls initialization functions defined in system.c and user.c source files, initializes global variables, and enables time-critical modules.
	If the watchdog timer was not disabled in the configuration bits file and was set to software enable, the watchdog timer can be disabled before initialization and then re-enabled before the infinite while loop.
	\\

	The infinite while loop, also referred to the main loop, is where the micro-controller spends the most of its time.
	Continuously updating code can be placed here such as checking to see if come event has happened.
	If the watchdog timer is enabled, this is a location for a watchdog servicing routine. For the PIC XC compilers it is as simple as the example below.
\begin{lstlisting}[language=c, frame=L]
#include <xc.h>
void main(void)
{
	while(1)
	{	/* Service the Watchdog Timer */
		CLRWDT();
	}
}
\end{lstlisting}
~\\

\subsubsection{Interrupt Service Routine}
	Instead of toggling the LED in the main loop, we will use the Timer 2 interrupt to handle the LED action in the interrupt service routine (ISR).
	When an interrupt is triggered by the Timer 2 module, the program is halted and enters the isr() function in interrupts.c, appendix~\ref{subsec:interrupts.c}, and then resumes the original process where it left off.
	When the program enters the ISR, we use one if statement block and will handle each interrupt one at a time.
	In the if statement we will check the status of the interrupt flag, TMR2IF, to see if that was the interrupt triggered.
	If it was the interrupt triggered, we will toggle the LED state.
	Before we leave the Timer 2 block, the interrupt flag will be cleared to prevent the interrupt from re-triggering the ISR before the next timer overflow.


\newpage
\allsectionsfont{\centering}
\section{Appendix}
\subsection{main.c} 
\label{subsec:main.c}
\lstinputlisting[language=c, frame=L, numbers=left]{MCU_example.X/main.c}
\newpage
\subsection{configuration\_bits.c} 
\label{subsec:configBits.c}
\lstinputlisting[language=c, frame=L, numbers=left]{MCU_example.X/configuration_bits.c}
\newpage
\subsection{system.h} 
\label{subsec:system.h}
\lstinputlisting[language=c, frame=L, numbers=left]{MCU_example.X/system.h}
\newpage
\subsection{system.c} 
\label{subsec:system.c}
\lstinputlisting[language=c, frame=L, numbers=left]{MCU_example.X/system.c}
\newpage
\subsection{user.h} 
\label{subsec:user.h}
\lstinputlisting[language=c, frame=L, numbers=left]{MCU_example.X/user.h}
\newpage
\subsection{user.c} 
\label{subsec:user.c}
\lstinputlisting[language=c, frame=L, numbers=left]{MCU_example.X/user.c}
\newpage
\subsection{interrupts.c} 
\label{subsec:interrupts.c}
\lstinputlisting[language=c, frame=L, numbers=left]{MCU_example.X/interrupts.c}

\newpage
\begin{thebibliography}{9}

\bibitem{microchip}
	Microchip,
	"Microchip Website"
	\emph{Microchip Technology Inc. 1998-2014},
	\url{http://www.microchip.com/}
	[Accessed: June 25,2014]

\bibitem{XC_sol}
	Microchip,
	"MPLAB XC Compilers"
	\emph{Microchip Technology Inc. 1998-2014},
	\url{http://www.microchip.com/pagehandler/en_us/devtools/mplabxc/}
	[Accessed: June 25,2014]
  
\bibitem{PIC16F1823}
	Microchip,
	"PIC16F1823"
	\emph{Microchip Technology Inc. 1998-2014},
	\url{http://www.microchip.com/wwwproducts/Devices.aspx?product=PIC16F1823}
	[Accessed: June 25,2014]
	
\bibitem{PIC16F1823_datasheet}
	Microchip,
	"PIC12(L)F1822/16(L)F1823"
	PIC16F1823 Datasheet
	[Revised: March 25, 2014]

\bibitem{PIC16F1823_errata}
	Microchip,
	"PIC12(L)F1822/16(L)F1823 Family Silicon Errata and Data Sheet Clarification"
	PIC16F1823 Errata
	[Revised: November 29, 2011]
	
\bibitem{eevblog_wiki}
	\url{http://www.eevblog.com/wiki/index.php?title=PCB_CAD_programs}
	[Accessed: June 25,2014]

\bibitem{sparkfun_eagle}
	Sparkfun Eagle Libraries on Github,
	\url{https://github.com/sparkfun/SparkFun-Eagle-Libraries}

\bibitem{allaboutcircuits}
	paultwang,
	"What is ground ?"
	All About Circuits.
	\url{forum.allaboutcircuits.com/}
	[Accessed: June 27, 2014]
	
\bibitem{xc8_guide}
	Microchip,
	"MPLAB XC8 C Compiler User's Guide"
	XC8 C Compiler Guide
	[Revised: November 29, 2011]

\bibitem{ledfig}
	Dawn Rorvik,
	"Electronics"
	evergreen.edu.
	\url{academic.evergreen.edu/}
	[Accessed: July 2, 2014]

\bibitem{pickit3_pinout}
	ero-baka,
	"flashing bootloader with PICKIT3"
	openpicus.com.
	\url{community.openpicus.com/},
	February 04, 2013
	[Accessed: July 8, 2014]
	
	
\end{thebibliography}
\end{document}