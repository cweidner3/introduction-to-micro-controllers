\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}A Note to Students}{3}{subsection.1.1}
\contentsline {section}{\numberline {2}Getting the Documentation}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}The Datasheet}{4}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}The Technical Reference Manual}{5}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}The Errata}{6}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Application Notes}{6}{subsection.2.4}
\contentsline {section}{\numberline {3}Creating the Circuit}{7}{section.3}
\contentsline {subsection}{\numberline {3.1}Simple LED Control Circuit}{7}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Main Connections}{8}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}LED Connection}{9}{subsubsection.3.1.2}
\contentsline {subsection}{\numberline {3.2}The Programming Tool}{10}{subsection.3.2}
\contentsline {section}{\numberline {4}Downloading the Software}{11}{section.4}
\contentsline {subsection}{\numberline {4.1}The Compiler}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Using the Integrated Development Environment}{11}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Creating a New MPLAB X Project}{11}{subsubsection.4.2.1}
\contentsline {section}{\numberline {5}Programming the Microcontroller}{12}{section.5}
\contentsline {subsection}{\numberline {5.1}Hardware Abstraction Layer}{12}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Blinking LED Program}{12}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}The Configuration Bits}{13}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Initialization Steps}{13}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}The Main Loop}{14}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}Interrupt Service Routine}{15}{subsubsection.5.2.4}
\contentsline {section}{\numberline {6}Appendix}{16}{section.6}
\contentsline {subsection}{\numberline {6.1}main.c}{16}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}configuration\_bits.c}{17}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}system.h}{18}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}system.c}{19}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}user.h}{20}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}user.c}{21}{subsection.6.6}
\contentsline {subsection}{\numberline {6.7}interrupts.c}{23}{subsection.6.7}
